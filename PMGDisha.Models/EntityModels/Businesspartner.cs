﻿using System;
using System.Collections.Generic;

namespace PMGDisha.Models.EntityModels
{
    public partial class Businesspartner
    {
        public Businesspartner()
        {
            Customers = new HashSet<Customers>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string SequenceNo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AuthToken { get; set; }
        public int? FkuserId { get; set; }
        public string MobileNo { get; set; }

        public User Fkuser { get; set; }
        public ICollection<Customers> Customers { get; set; }
    }
}
