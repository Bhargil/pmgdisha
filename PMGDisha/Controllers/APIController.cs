﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMGDisha.Services;
using PMGDisha.Infrastructure;
using PMGDisha.Models.CustomModels;

namespace PMGDisha.Controllers
{
    [Route("api")]
    [ApiController]
    public class APIController : ControllerBase
    {
        BusinessPartnerServices businessPartnerServices = new BusinessPartnerServices();

        [HttpPost]
        [Route("partner/auth")]
        public async Task<IActionResult> PartnerAuth(string username, string password)
        {
            try
            {
                var _resp = businessPartnerServices.CheckAuthPartner(username, password);
                if (!string.IsNullOrEmpty(_resp))
                {
                    List<string> respmodel = new List<string>();
                    respmodel.Add(_resp);
                    var response = ResponceMethod<string>.SuccessResponse(StatusCodes.Status200OK, "Auth Successfully", respmodel);
                    return Ok(response);
                }
                else
                {
                    var result = ResponceMethod<string>.GetResponce(StatusCodes.Status400BadRequest, "Invalid login key", null);
                    return BadRequest(result);
                }

            }
            catch (Exception ex)
            {
                var result = ResponceMethod<string>.GetResponce(StatusCodes.Status400BadRequest, "Error: " + ex.Message, null);
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("partner/customerlist")]
        public async Task<IActionResult> GetPartnerCustomer()
        {
            try
            {
                string _token = Request.AuthenticationToken();
                var resp = businessPartnerServices.GetCustomerByPartnerToken(_token);
                if (resp != null)
                {
                    var response = ResponceMethod<CustomerByPartner>.SuccessResponse(StatusCodes.Status200OK, "Records get successfully", resp);
                    return Ok(response);
                }
                else
                {
                    var _resp = new List<CustomerByPartner>();
                    var result = ResponceMethod<CustomerByPartner>.GetResponce(StatusCodes.Status400BadRequest, "No Data found!", _resp);
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                var result = ResponceMethod<string>.GetResponce(StatusCodes.Status400BadRequest, "Error: " + ex.Message, null);
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("partner/customer")]
        public async Task<IActionResult> GetCustomerByCustomerCode(string code)
        {
            try
            {
                string _token = Request.AuthenticationToken();
                var resp = businessPartnerServices.GetCustomerByCode(code);
                if (resp != null)
                {
                    List<CustomerByPartner> responsemodel = new List<CustomerByPartner>();
                    responsemodel.Add(resp);
                    var response = ResponceMethod<CustomerByPartner>.SuccessResponse(StatusCodes.Status200OK, "Records get successfully", responsemodel);
                    return Ok(response);
                }
                else
                {
                    var result = ResponceMethod<CustomerByPartner>.GetResponce(StatusCodes.Status400BadRequest, "No Data found!", new List<CustomerByPartner>());
                    return BadRequest(result);
                }
            }
            catch (Exception ex)
            {
                var result = ResponceMethod<string>.GetResponce(StatusCodes.Status400BadRequest, "Error: " + ex.Message, null);
                return BadRequest(result);
            }
        }
    }
}