﻿using System;
using System.Collections.Generic;

namespace PMGDisha.Models.EntityModels
{
    public partial class Customers
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public int FkbusinessPartnerId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string AadharNo { get; set; }
        public string RationNo { get; set; }
        public string VillageName { get; set; }
        public string Image1Url { get; set; }
        public int? Image1Quality { get; set; }
        public string Image2Url { get; set; }
        public int? Image2Quality { get; set; }
        public string Image3Url { get; set; }
        public int? Image3Quality { get; set; }
        public string Image4Url { get; set; }
        public int? Image4Quality { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Image1Iurl { get; set; }
        public string Image2Iurl { get; set; }
        public string Image3Iurl { get; set; }
        public string Image4Iurl { get; set; }

        public Businesspartner FkbusinessPartner { get; set; }
    }
}
