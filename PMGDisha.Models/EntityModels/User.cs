﻿using System;
using System.Collections.Generic;

namespace PMGDisha.Models.EntityModels
{
    public partial class User
    {
        public User()
        {
            Businesspartner = new HashSet<Businesspartner>();
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public bool? Isactive { get; set; }
        public DateTime Createddate { get; set; }
        public string UserType { get; set; }

        public ICollection<Businesspartner> Businesspartner { get; set; }
    }
}
