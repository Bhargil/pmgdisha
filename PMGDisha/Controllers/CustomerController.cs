﻿using System;
using Microsoft.AspNetCore.Mvc;
using PMGDisha.Services;
using Microsoft.AspNetCore.Http;
using PMGDisha.Models.EntityModels;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace PMGDisha.Controllers
{
    public class CustomerController : Controller
    {
        private static readonly log4net.ILog log =
          log4net.LogManager.GetLogger(typeof(Program));
        public IActionResult Index()
        {
            int page = 1; int pagerow = 500; string sortby = "CreatedDate desc"; string search = null;
            CustomerServices customerServices = new CustomerServices();
            //if (TempData["USERNAME"] != null)
            {
                log.Info("shree ganesh");
                int _uId = customerServices.GetPIdByFK(Convert.ToInt32(HttpContext.Session.GetInt32("UID")));
                var _list = customerServices.GetCustomers(page, pagerow, sortby, search, _uId);
                return View(_list);
            }
        }
        public IActionResult ManageCustomer()
        {
            return View();
        }
        public IActionResult FilterList(int page = 1, int pagerow = 25, string sortby = "CreatedDate descending", string search = null)
        {
            CustomerServices customerServices = new CustomerServices();
            if (HttpContext.Session.GetInt32("UID") != null)
            {
                int _uId = customerServices.GetPIdByFK(Convert.ToInt32(HttpContext.Session.GetInt32("UID")));
                var _list = customerServices.GetCustomers(page, pagerow, sortby, search, _uId);
                return RedirectToAction("Index", "Customer", _list);
            }
            return PartialView("~/Views/Customer/Partial/_List.cshtml", null);
        }

        [HttpPost]
        public IActionResult ManageCustomerDetails(string CustCode, string Name, string AadharNo, string VillageName, string RationNo, string img1Base64, string IMAGE_QUALITY_1,
            string img2Base64, string IMAGE_QUALITY_2, string img3Base64, string IMAGE_QUALITY_3, string img4Base64, string IMAGE_QUALITY_4)
        {

            try
            {
                CustomerServices customerServices = new CustomerServices();
                log.Debug("Insert Customer Start :" + CustCode);
                string _code = string.Empty;
                if (string.IsNullOrEmpty(CustCode))
                {
                    _code = GeneralServices.GenerateAuthToken(20).ToLower();
                }
                else
                {
                    _code = CustCode;
                }
                log.Debug("Start to Save Customer Image :" + CustCode);
                string _path1 = string.Empty;
                string _Ipath1 = string.Empty;
                if (!string.IsNullOrEmpty(img1Base64))
                {
                    _path1 = SaveFingerPrint(img1Base64, _code + "_1");
                    _Ipath1 = Path.GetFileName(_path1).Replace(".png", "") + "_i.png";
                }

                string _path2 = string.Empty;
                string _Ipath2 = string.Empty;
                if (!string.IsNullOrEmpty(img2Base64))
                {
                    _path2 = SaveFingerPrint(img2Base64, _code + "_2");
                    _Ipath2 = Path.GetFileName(_path2).Replace(".png", "") + "_i.png";
                }
                string _path3 = string.Empty;
                string _Ipath3 = string.Empty;
                if (!string.IsNullOrEmpty(img3Base64))
                {
                    _path3 = SaveFingerPrint(img3Base64, _code + "_3");
                    _Ipath3 = Path.GetFileName(_path3).Replace(".png", "") + "_i.png";
                }
                string _path4 = string.Empty;
                string _Ipath4 = string.Empty;
                if (!string.IsNullOrEmpty(img4Base64))
                {
                    _path4 = SaveFingerPrint(img4Base64, _code + "_4");
                    _Ipath4 = Path.GetFileName(_path4).Replace(".png", "") + "_i.png";
                }

                Customers customers = new Customers
                {
                    Name = Name,
                    AadharNo = AadharNo,
                    Code = _code,
                    CreatedDate = DateTime.Now,
                    FkbusinessPartnerId = customerServices.GetPIdByFK(Convert.ToInt32(HttpContext.Session.GetInt32("UID"))),
                    Image1Quality = (!string.IsNullOrEmpty(IMAGE_QUALITY_1) ? Convert.ToInt32(IMAGE_QUALITY_1) : 0),
                    Image1Url = _path1,
                    Image2Quality = (!string.IsNullOrEmpty(IMAGE_QUALITY_2) ? Convert.ToInt32(IMAGE_QUALITY_2) : 0),
                    Image2Url = _path2,
                    Image3Quality = (!string.IsNullOrEmpty(IMAGE_QUALITY_3) ? Convert.ToInt32(IMAGE_QUALITY_3) : 0),
                    Image3Url = _path3,
                    Image4Quality = (!string.IsNullOrEmpty(IMAGE_QUALITY_4) ? Convert.ToInt32(IMAGE_QUALITY_4) : 0),
                    Image4Url = _path4,
                    RationNo = RationNo,
                    VillageName = VillageName,
                    Image1Iurl = _Ipath1,
                    Image2Iurl = _Ipath2,
                    Image3Iurl = _Ipath3,
                    Image4Iurl = _Ipath4,
                };
                if (string.IsNullOrEmpty(CustCode))
                {
                    log.Debug("Insert Customer Start");
                    customerServices.InsertCustomers(customers);
                }
                else
                {
                    log.Debug("Update Customer Start");
                    customerServices.UpdateCustomers(customers);
                }
                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                log.Error("Insert Customer exception :" + ex.Message);
                return RedirectToAction("Index", ex.Message.ToString());
            }

        }
        [HttpGet]
        public Models.CustomModels.CustomerCustomModel getdetailsbycode(string code)
        {
            CustomerServices customerServices = new CustomerServices();
            return customerServices.GetCustomer(code);
        }
        private string SaveFingerPrint(string base64String, string code)
        {
            string imageName = code + ".png";
            try
            {
                string path = ConstantVariables.ImageFolder; //Path
                log.Debug("Path : " + path);
                //Check if directory exist
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path); //Create directory if it doesn't exist
                }


                //set the image path
                string imgPath = Path.Combine(path, imageName);
                log.Debug("ImagePath : " + imgPath);
                byte[] imageBytes = Convert.FromBase64String(base64String);

                System.IO.File.WriteAllBytes(imgPath, imageBytes);
                log.Debug("Image Saved : " + imgPath);

                SetInvertImage(imageBytes, code);
            }
            catch (Exception ex)
            {
                log.Error("Image Save exception :" + ex.Message.ToString());
            }

            return imageName;
        }
        private void SetInvertImage(byte[] _imgByte, string _imgName)
        {
            try
            {
                log.Debug("SetInvertImage Start : " + _imgName);
                Bitmap _currentBitmap;
                string _path = ConstantVariables.ImageFolder;
                using (var ms = new MemoryStream(_imgByte))
                {
                    _currentBitmap = new Bitmap(ms);
                }
                Bitmap temp = _currentBitmap;                
                Bitmap bmap = new Bitmap(_currentBitmap,316,354);
                _currentBitmap.SetResolution(200F, 200F);
                for (int i = 0; i < bmap.Width; i++)
                {
                    for (int j = 0; j < bmap.Height; j++)
                    {
                        Color p = bmap.GetPixel(i, j);
                        //extract ARGB value from p
                        int a = p.A;
                        int r = p.R;
                        int g = p.G;
                        int b = p.B;

                        //find negative value
                        r = 255 - r;
                        g = 255 - g;
                        b = 255 - b;

                        //set new ARGB value in pixel
                        bmap.SetPixel(i, j, Color.FromArgb(a, r, g, b));                        
                    }
                }
                _currentBitmap = (Bitmap)bmap.Clone();
                _currentBitmap.RotateFlip(RotateFlipType.Rotate180FlipXY);
               
                MemoryStream _ms = new MemoryStream();
                _currentBitmap.Save(_ms, ImageFormat.Bmp);
                byte[] bmpBytes = _ms.ToArray();

                string imageName = _imgName + "_i" + ".png";

                //set the image path
                string imgPath = Path.Combine(_path, imageName);
                log.Debug("SetInvertImage ImagePath : " + imgPath);
                System.IO.File.WriteAllBytes(imgPath, bmpBytes);
                log.Debug("SetInvertImage ImagePath done");
            }
            catch (Exception ex)
            {
                log.Error("SetInvertImage Exception : " + ex.Message);
            }
        }


    }

}