﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMGDisha.Models.CustomModels;
using PMGDisha.Models.EntityModels;
using PMGDisha.Services;

namespace PMGDisha.Controllers
{
    public class PartnerController : Controller
    {
        public IActionResult Index()
        {
            BusinessPartnerServices partnerServices = new BusinessPartnerServices();

            int _uId = Convert.ToInt32(HttpContext.Session.GetInt32("UID"));
            var _list = partnerServices.GetPartner(1, 500, "Id descending", string.Empty, _uId);
            return View(_list);

        }

        public IActionResult GetAllpartner()
        {
            BusinessPartnerServices partnerServices = new BusinessPartnerServices();
            int _uId = Convert.ToInt32(HttpContext.Session.GetInt32("UID"));
            var _list = partnerServices.GetPartner(1, 5000, "Id descending", string.Empty, _uId);
            return PartialView("~/Views/Partner/Partial/_List.cshtml", _list);
        }


        public IActionResult ManagePartner(int ID)
        {
            BusinessPartnerServices partnerServices = new BusinessPartnerServices();
            Partner bpartner = new Partner();
            if (ID > 0)
            {
                bpartner = partnerServices.GetPartnerByID(ID);
            }

            return PartialView("~/Views/Partner/_ManagePartner.cshtml", bpartner);
        }

        public IActionResult DeletePartner(int ID)
        {
            BusinessPartnerServices partnerServices = new BusinessPartnerServices();

            if (ID > 0)
            {
                partnerServices.DeletePartner(ID);
            }

            return RedirectToAction("Index", "Partner");
        }

        [HttpPost]
        public IActionResult ManagepartnerDetails(Models.CustomModels.Partner bpartner)
        {
            BusinessPartnerServices partnerServices = new BusinessPartnerServices();
            Businesspartner p = new Businesspartner()
            {
                Address1 = bpartner.Address1,
                Address2 = bpartner.Address2,
                City = bpartner.City,
                Id = bpartner.PartnerId,
                MobileNo = bpartner.MobileNo,
                Name = bpartner.Name,
                SequenceNo = bpartner.SequenceNo,
                State = bpartner.State
            };
            if (bpartner.PartnerId > 0)
            {
                partnerServices.UpdateBusinessPartner(p, bpartner.Password);
            }
            else
                partnerServices.InsertBusinessPartner(p, bpartner.Password);

            return RedirectToAction("Index", "Partner");
        }
    }


}