﻿using Microsoft.AspNetCore.Http;
using PMGDisha.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace PMGDisha.Infrastructure
{
    public static class SystemIdentity
    {
        public static string AuthenticationToken(this HttpRequest httpRequest)
        {
            return Convert.ToString(httpRequest.Headers["Authorization"]);
        }       
        
    }    
}
