﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMGDisha.Models.CustomModels
{
    public class Partner
    {

        public int PartnerId { get; set; }
        public string Name { get; set; }
        public string SequenceNo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AuthToken { get; set; }
        public int? FkuserId { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }

    }
}
