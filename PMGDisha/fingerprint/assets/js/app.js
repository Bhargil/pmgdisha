

function app_load() {

}

var flag = 0;
var quality = 99; //(1 to 100) (recommanded minimum 55)
var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

function Capture(no) {
    try {
        document.getElementById('imgFinger_' + no).src = "data:image/bmp;base64,";
        document.getElementById('imgFingerHor_' + no).src = "data:image/bmp;base64,";
        document.getElementById('IMAGE_QUALITY_' + no).value = "";


        var res = CaptureFinger(quality, timeout);
        if (res.httpStaus) {
            flag = 1;
            // document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;

            if (res.data.ErrorCode == "0") {

                document.getElementById('imgFinger_' + no).src = "data:image/bmp;base64," + res.data.BitmapData;
                document.getElementById('imgFingerHor_' + no).src = "data:image/bmp;base64," + res.data.BitmapData;
                document.getElementById('IMAGE_QUALITY_' + no).value = res.data.Quality;
                setTimeout(function () {
                    html2canvas([document.getElementById('div-imgFingerHor_' + no)], {
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL('image/png');
                            var imgdata = data.replace(/^data:image\/(png|jpg);base64,/, "");
                            document.getElementById('imgFinger_Hor_png_' + no).src = data;
                            document.getElementById('IMAGE_DATA_' + no).value = imgdata;
                        }
                    });
                }, 2000);

                //BitmapDataHor
                // rotateBase64Image("data:image/png;base64," + res.data.BitmapData);
            }
        }
        else {
            alert(res.err);
        }
    }
    catch (e) {
        alert(e);
    }
    return false;
}