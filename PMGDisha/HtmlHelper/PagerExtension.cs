﻿using System;
using System.Text;
using PMGDisha.Services;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Html;

namespace PMGDisha
{
    public static class PagerExtension
    {
        public static HtmlString Pager( string url, int currentPage, int totalItem, int itemPerPage, string sortBy = "")
        {
            int totalPage = ConvertTo.Integer(Math.Ceiling(ConvertTo.Double(totalItem) / ConvertTo.Double(itemPerPage)));
            if (totalPage > 1)
            {
                var firstrecordindex = (currentPage - 1) * itemPerPage;
                var lastrecordindex = firstrecordindex + itemPerPage;
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='col-sm-4'>");
                sb.Append("<span>Showing " + (firstrecordindex + 1) + " to " + (lastrecordindex > totalItem ? totalItem : lastrecordindex) + " of " + totalItem + " entries</span>");
                sb.Append("</div>");
                sb.Append("<div class='col-sm-8'>");
                sb.Append("<div class='btn-group pull-right pager-link'>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='1'  table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + (currentPage - 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i></a>");
                if (currentPage <= 5)
                {
                    int looping = totalPage > 9 ? 9 : totalPage;
                    for (int page = 1; page <= looping; page++)
                    {
                        sb.Append("<a class='btn btn-white " + (currentPage == page ? "active" : "") + "' href='" + url + "' data-page='" + page + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + page + "</a>");
                    }
                }
                else
                {
                    int start = -4, end = 4;
                    if (currentPage + 4 > totalPage)
                    {
                        start = start - (currentPage + 4 - totalPage);
                        end = end - (currentPage + 4 - totalPage);
                    }
                    for (int page = start; page <= end; page++)
                    {

                        sb.Append("<a class='btn btn-white " + (page == 0 ? "active" : "") + "' href='" + url + "' data-page='" + (currentPage + page) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + (currentPage + page) + "</a>");
                    }
                }
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + (currentPage + 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i> </a>");
                sb.Append("<a class='btn btn-white' href='" + url + "' data-page='" + totalPage + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i> </a>");
                sb.Append("</div>");
                sb.Append("</div>");
                HtmlString htmlString = new HtmlString(sb.ToString());
                return htmlString;
            }
            HtmlString _htmlString = new HtmlString("");
            return _htmlString;
        }

        public static HtmlString PagerFrontend(this HtmlHelper htmlHelper, string url, int currentPage, int totalItem, int itemPerPage, string sortBy = "")
        {
            int totalPage = ConvertTo.Integer(Math.Ceiling(ConvertTo.Double(totalItem) / ConvertTo.Double(itemPerPage)));
            if (totalPage > 1)
            {
                var firstrecordindex = (currentPage - 1) * itemPerPage;
                var lastrecordindex = firstrecordindex + itemPerPage;
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='col-md-4 col-sm-4 m-top-6'>");
                sb.Append("<span>Showing " + (firstrecordindex + 1) + " to " + (lastrecordindex > totalItem ? totalItem : lastrecordindex) + " of " + totalItem + " entries</span>");
                sb.Append("</div>");
                sb.Append("<div class='col-md-8 col-sm-8 m-p-15'>");
                sb.Append("<div class='shop-header-right'>");
                sb.Append("<nav class='product-pagination'>");
                sb.Append("<nav class='pagination pull-right'>");
                sb.Append("<ul class='pagination pager-link'>");
                //sb.Append("<li><a  href='" + url + "' data-page='1'  table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i></a> </li>");
                sb.Append("<li><a  href='" + url + "' data-page='" + (currentPage - 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-left'></i></a></li>");
                if (currentPage <= 5)
                {
                    int looping = totalPage > 9 ? 9 : totalPage;
                    for (int page = 1; page <= looping; page++)
                    {
                        sb.Append(" <li class='" + (currentPage == page ? "active" : "") + "'><a  href='" + url + "' data-page='" + page + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + page + "</a></li>");
                    }
                }
                else
                {
                    int start = -4, end = 4;
                    if (currentPage + 4 > totalPage)
                    {
                        start = start - (currentPage + 4 - totalPage);
                        end = end - (currentPage + 4 - totalPage);
                        if (start < totalPage)
                        {
                            start = -currentPage + 1;
                        }
                    }
                    for (int page = start; page <= end; page++)
                    {

                        sb.Append(" <li class='" + (page == 0 ? "active" : "") + "'><a  href='" + url + "' data-page='" + (currentPage + page) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'>" + (currentPage + page) + "</a></li>");
                    }
                }
                sb.Append(" <li><a class='' href='" + url + "' data-page='" + (currentPage + 1) + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i> </a></li>");
                //sb.Append(" <li><a class='btn btn-white' href='" + url + "' data-page='" + totalPage + "' table-sortby='" + sortBy + "' item-page='" + itemPerPage + "'><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i> </a> </li>");
                sb.Append("</ul>");
                sb.Append("</nav>");
                sb.Append("</div>");
                sb.Append("</div>");
                HtmlString htmlString = new HtmlString(sb.ToString());
                return htmlString;
            }
            HtmlString _htmlString = new HtmlString("");
            return _htmlString;
        }
    }
}