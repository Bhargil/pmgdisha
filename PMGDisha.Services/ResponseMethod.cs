﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PMGDisha.Services
{
    public class ResponceMethod<T>
    {
        public static Responsemodel<T> GetResponce(int statusCode, string errorMessage, IEnumerable<T> responseModel)
        {
            Responsemodel<T> obj = new Responsemodel<T>
            {
                Status = statusCode,
                Message = errorMessage,
                Data = responseModel
            };
            return obj;
        }
        public static Responsemodel<T> SuccessResponse(int status, string message, IEnumerable<T> responseModel)
        {
            Responsemodel<T> response = new Responsemodel<T>
            {
                Status = status,
                Message = message,
                Data = responseModel
            };
            return response;
        }
    }

    public class Responsemodel<T>
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}
