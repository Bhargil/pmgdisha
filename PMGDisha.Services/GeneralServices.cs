﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PMGDisha.Services
{
    public class GeneralServices
    {
        public static string GenerateAuthToken(int charLength)
        {
            var random = new Random();
            var resultToken = new string(Enumerable.Repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", charLength)
                                    .Select(token => token[random.Next(token.Length)]).ToArray());
            return resultToken.ToString();
        }
    }
}
