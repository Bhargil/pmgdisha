﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PMGDisha.Models.CustomModels
{
    public class CustomerByPartner
    {
        public string Code { get; set; }
        public int FKBusinessPartnerId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string AadharNo { get; set; }
        public string RationNo { get; set; }
        public string VillageName { get; set; }
        public string Image1URL { get; set; }
        public int? Image1Quality { get; set; }
        public string Image2URL { get; set; }
        public int? Image2Quality { get; set; }
        public string Image3URL { get; set; }
        public int? Image3Quality { get; set; }
        public string Image4URL { get; set; }
        public int? Image4Quality { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
    public class CustomerModel
    {
        [Required(ErrorMessage = "Enter Name", AllowEmptyStrings = false)]
        public string Name { get; set; }

        public string Code { get; set; }

        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Enter Name", AllowEmptyStrings = false)]

        public string AadharNo { get; set; }
        public string RationNo { get; set; }
        public string VillageName { get; set; }
        public string Image1URL { get; set; }
        public int? Image1Quality { get; set; }
        public string Image2URL { get; set; }
        public int? Image2Quality { get; set; }
        public string Image3URL { get; set; }
        public int? Image3Quality { get; set; }
        public string Image4URL { get; set; }
        public int? Image4Quality { get; set; }
    }
    public class CustomerCustomModel
    {
        public string Name { get; set; }
        public string AadharNo { get; set; }
        public string VillageName { get; set; }
        public string RationNo { get; set; }

        public string img1Base64 { get; set; }
        public string IMAGE_QUALITY_1 { get; set; }

        public string img2Base64 { get; set; }
        public string IMAGE_QUALITY_2 { get; set; }

        public string img3Base64 { get; set; }
        public string IMAGE_QUALITY_3 { get; set; }

        public string img4Base64 { get; set; }
        public string IMAGE_QUALITY_4 { get; set; }

    }
}
