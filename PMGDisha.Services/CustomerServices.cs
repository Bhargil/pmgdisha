﻿using PMGDisha.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using PMGDisha.Models.CustomModels;
using Z.EntityFramework.Plus;

namespace PMGDisha.Services
{
    public class CustomerServices
    {
        public PagerList<Customers> GetCustomers(int pageNumber, int pageRows, string orderBy, string searchtext, int _fkUserId)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                PagerList<Customers> paginationObject = null;
                var query = (from cust in db.Customers
                             join b in db.Businesspartner on cust.FkbusinessPartnerId equals b.Id
                             where cust.FkbusinessPartnerId == _fkUserId
                             select cust).AsQueryable();
                if (!string.IsNullOrEmpty(searchtext))
                {
                    query = query.Where(w => w.Name.Contains(searchtext) || w.AadharNo.Contains(searchtext) || w.VillageName.Contains(searchtext) || w.RationNo.Contains(searchtext));
                }
                paginationObject = query.ToPagerListOrderBy(pageNumber, pageRows, orderBy);
                return paginationObject;
            }

        }
        public void InsertCustomers(Customers _cust)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                db.Customers.Add(_cust);
                db.SaveChanges();
            }
        }
        public void UpdateCustomers(Customers _cust)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                db.Customers.Where(w => w.Code == _cust.Code).Update(up => new Customers
                {
                    AadharNo = _cust.AadharNo,
                    Image1Quality = _cust.Image1Quality,
                    Image1Url = _cust.Image1Url,
                    Image2Quality = _cust.Image2Quality,
                    Image2Url = _cust.Image2Url,
                    Image3Quality = _cust.Image3Quality,
                    Image3Url = _cust.Image3Url,
                    Image4Quality = _cust.Image4Quality,
                    Image4Url = _cust.Image4Url,
                    Name = _cust.Name,
                    RationNo = _cust.RationNo,
                    VillageName = _cust.VillageName
                });
            }

        }
        public CustomerCustomModel GetCustomer(string _code)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                return db.Customers.Where(w => w.Code == _code).Select(s => new CustomerCustomModel
                {
                    AadharNo = s.AadharNo,
                    IMAGE_QUALITY_1 = Convert.ToString(s.Image1Quality ?? 0),
                    IMAGE_QUALITY_2 = Convert.ToString(s.Image2Quality ?? 0),
                    IMAGE_QUALITY_3 = Convert.ToString(s.Image3Quality ?? 0),
                    IMAGE_QUALITY_4 = Convert.ToString(s.Image4Quality ?? 0),
                    img1Base64 = !string.IsNullOrEmpty(s.Image1Url) ? GetBase64FromImagePath(ConstantVariables.ImageFolder + s.Image1Url) : "",
                    img2Base64 = !string.IsNullOrEmpty(s.Image1Url) ? GetBase64FromImagePath(ConstantVariables.ImageFolder + s.Image2Url) : "",
                    img3Base64 = !string.IsNullOrEmpty(s.Image1Url) ? GetBase64FromImagePath(ConstantVariables.ImageFolder + s.Image3Url) : "",
                    img4Base64 = !string.IsNullOrEmpty(s.Image1Url) ? GetBase64FromImagePath(ConstantVariables.ImageFolder + s.Image4Url) : "",
                    Name = s.Name,
                    VillageName = s.VillageName
                }).FirstOrDefault();
            }
        }
        public int GetPIdByFK(int _FKUserId)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                return db.Businesspartner.Where(w => w.FkuserId == _FKUserId).Select(s => s.Id).FirstOrDefault();
            }
        }
        private string GetBase64FromImagePath(string _path)
        {
            byte[] imageArray = System.IO.File.ReadAllBytes(_path);
            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            return base64ImageRepresentation;
        }
    }
}
