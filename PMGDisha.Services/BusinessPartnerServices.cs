﻿using PMGDisha.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Z.EntityFramework.Plus;
using PMGDisha.Models.CustomModels;

namespace PMGDisha.Services
{
    public class BusinessPartnerServices
    {
        public PagerList<Businesspartner> GetPartner(int pageNumber, int pageRows, string orderBy, string searchtext, int _fkUserId)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                PagerList<Businesspartner> paginationObject = null;
                var query = (from b in db.Businesspartner
                             select b).AsQueryable();
                // where b.FkuserId == _fkUserId
                if (!string.IsNullOrEmpty(searchtext))
                {
                    query = query.Where(w => w.Name.Contains(searchtext) || w.MobileNo.Contains(searchtext) || w.City.Contains(searchtext) || w.Address1.Contains(searchtext));
                }
                paginationObject = query.ToPagerListOrderBy(pageNumber, pageRows, orderBy);
                return paginationObject;
            }

        }

        public Partner GetPartnerByID(int partnerID)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                var query = (from b in db.Businesspartner
                             join u in db.User on b.FkuserId equals u.Id
                             where b.Id == partnerID
                             select new Partner
                             {
                                 PartnerId = b.Id,
                                 FkuserId = b.FkuserId,
                                 Address1 = b.Address1,
                                 Address2 = b.Address2,
                                 City = b.City,
                                 MobileNo = b.MobileNo,
                                 Name = b.Name,
                                 SequenceNo = b.SequenceNo,
                                 State = b.State,
                                 Password = u.Password
                             }).FirstOrDefault();

                return query;
            }

        }

        // Generate a random string with a given size    
        public string RandomString()
        {
            int size = 30;
            bool lowerCase = true;
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public void DeletePartner(int partnerID)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                db.Businesspartner.Where(x => x.Id == partnerID).Delete();
            }
        }

        public string CheckAuthPartner(string _username, string _password)
        {
            string token = string.Empty;
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                var PartnerExists = (from u in db.User
                                     join b in db.Businesspartner on u.Id equals b.FkuserId
                                     where u.UserId == _username && u.Password == _password
                                     select b).FirstOrDefault();
                if (PartnerExists != null)
                {
                    token = GeneralServices.GenerateAuthToken(50);
                    db.Businesspartner.Where(w => w.Id == PartnerExists.Id).Update(up => new Businesspartner
                    {
                        AuthToken = token
                    });
                }

            }
            return token;
        }
        public List<CustomerByPartner> GetCustomerByPartnerToken(string _authToken)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                var partner = db.Businesspartner.Where(w => w.AuthToken == _authToken).FirstOrDefault();
                if (partner != null)
                {
                    return db.Customers.Where(w => w.FkbusinessPartnerId == partner.Id).Select(s => new CustomerByPartner
                    {
                        AadharNo = s.AadharNo,
                        Code = s.Code,
                        FKBusinessPartnerId = s.FkbusinessPartnerId,
                        Image1Quality = s.Image1Quality,
                        Image1URL = (!string.IsNullOrEmpty(s.Image1Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image1Iurl : string.Empty),
                        Image2Quality = s.Image2Quality,
                        Image2URL = (!string.IsNullOrEmpty(s.Image2Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image2Iurl : string.Empty),
                        Image3Quality = s.Image3Quality,
                        Image3URL = (!string.IsNullOrEmpty(s.Image3Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image3Iurl : string.Empty),
                        Image4Quality = s.Image4Quality,
                        Image4URL = (!string.IsNullOrEmpty(s.Image4Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image4Iurl : string.Empty),
                        MobileNo = s.MobileNo,
                        Name = s.Name,
                        RationNo = s.RationNo,
                        VillageName = s.VillageName,
                        CreatedDate = s.CreatedDate
                    }).OrderByDescending(o => o.CreatedDate).ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public CustomerByPartner GetCustomerByCode(string _code)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                return db.Customers.Where(w => w.Code == _code).Select(s => new CustomerByPartner
                {
                    AadharNo = s.AadharNo,
                    Code = s.Code,
                    FKBusinessPartnerId = s.FkbusinessPartnerId,
                    Image1Quality = s.Image1Quality,
                    Image1URL = (!string.IsNullOrEmpty(s.Image1Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image1Iurl : string.Empty),
                    Image2Quality = s.Image2Quality,
                    Image2URL = (!string.IsNullOrEmpty(s.Image2Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image2Iurl : string.Empty),
                    Image3Quality = s.Image3Quality,
                    Image3URL = (!string.IsNullOrEmpty(s.Image3Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image3Iurl : string.Empty),
                    Image4Quality = s.Image4Quality,
                    Image4URL = (!string.IsNullOrEmpty(s.Image4Iurl) ? ConstantVariables.APIURL + ConstantVariables.ImageFolder + s.Image4Iurl : string.Empty),
                    MobileNo = s.MobileNo,
                    Name = s.Name,
                    RationNo = s.RationNo,
                    VillageName = s.VillageName
                }).FirstOrDefault();
            }
        }


        public void InsertBusinessPartner(Businesspartner bs, string password)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                Businesspartner businesspartner = new Businesspartner()
                {
                    Address1 = bs.Address1,
                    Address2 = bs.Address2,
                    City = bs.City,
                    Name = bs.Name,
                    SequenceNo = bs.SequenceNo,
                    State = bs.State,
                    MobileNo = bs.MobileNo,
                    AuthToken = RandomString()
                };
                db.Businesspartner.Add(businesspartner);
                db.SaveChanges();

                User user = new User()
                {
                    UserId = bs.MobileNo,
                    Password = password,
                    Createddate = DateTime.Now,
                    Isactive = true,
                    UserType = "PARTNER"
                };
                db.User.Add(user);
                db.SaveChanges();

                db.Businesspartner.Where(w => w.Id == businesspartner.Id).Update(up => new Businesspartner
                {
                    FkuserId = user.Id
                });
            }
        }

        public void UpdateBusinessPartner(Businesspartner bs, string _password)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                db.Businesspartner.Where(w => w.Id == bs.Id).Update(up => new Businesspartner
                {
                    Address1 = bs.Address1,
                    Address2 = bs.Address2,
                    City = bs.City,
                    MobileNo = bs.MobileNo,
                    Name = bs.Name,
                    State = bs.State
                });

                try
                {
                    int _BId = (int)db.Businesspartner.Where(w => w.Id == bs.Id).Select(s => s.FkuserId).FirstOrDefault();
                    db.User.Where(w => w.Id == _BId).Update(up => new User
                    {
                        UserId = bs.MobileNo,
                        Password = _password
                    });
                }
                catch (Exception ex) { }

            }
        }
    }
}
