﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMGDisha
{
    public static class ActiveClass
    {
        public static string IsActive(this HtmlHelper html, string action, string controller)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (string.IsNullOrEmpty(controller))
            { controller = currentController; }

            if (string.IsNullOrEmpty(action))
            { action = "Index"; }

            return controller.ToLower() == currentController.ToLower() && action.ToLower() == currentAction.ToLower() ? cssClass : string.Empty;
        }
    }
}