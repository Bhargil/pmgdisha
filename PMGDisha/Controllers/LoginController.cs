﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PMGDisha.Models.CustomModels;
using PMGDisha.Services;

namespace PMGDisha.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            AuthServices authServices = new AuthServices();
          
            var _user = authServices.GetAuth(model.UserId, model.Password);
            if (_user != null)
            {
                HttpContext.Session.SetInt32("UID", _user.UserId);
                HttpContext.Session.SetString("USERNAME", _user.UserName);
                HttpContext.Session.SetString("USERTYPE", _user.UserType);
                TempData["USERNAME"] = HttpContext.Session.GetString("USERNAME");
                if (_user.UserType == "ADMIN")
                {
                    return RedirectToAction("Index", "Partner");
                }
                else if (_user.UserType == "PARTNER")
                {
                    return RedirectToAction("Index", "Customer");
                }
              
            }
            return View();
        }
    }
}