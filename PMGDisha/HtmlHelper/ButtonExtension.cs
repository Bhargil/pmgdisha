﻿using PMGDisha.Services;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PMGDisha.helpers
{
    public static class ButtonExtension
    {
        public static IHtmlContent Sorting(this HtmlHelper htmlHelper, string url = "", string sortingBy = "", string htmlString = "", string currentSortingBy = "", object htmlAttribute = null)
        {
            string descending = "descending";
            TagBuilder tag = new TagBuilder("th");
            tag.MergeAttribute("data-sortby", sortingBy);
            tag.MergeAttribute("data-role", "columnheader");
            tag.MergeAttribute("data-url", url);
            tag.AddCssClass("custom-sorting");
            if (!string.IsNullOrWhiteSpace(currentSortingBy))
            {
                string[] parameter = currentSortingBy.Trim().Split(' ');
                if (parameter.Length == 2)
                {
                    if (parameter[0].ToLower() == sortingBy.ToLower() && parameter[1].ToLower() == descending)
                    {
                        tag.AddCssClass("sorting_desc");
                        tag.MergeAttribute("aria-sort", "");
                    }
                    else
                    {
                        tag.AddCssClass("sorting");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                }
                else
                {
                    if (parameter[0].ToLower() == sortingBy.ToLower())
                    {
                        tag.AddCssClass("sorting_asc");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                    else
                    {
                        tag.AddCssClass("sorting");
                        tag.MergeAttribute("aria-sort", "" + descending);
                    }
                }
            }
            else
            {
                tag.AddCssClass("sorting");
                tag.MergeAttribute("aria-sort", "" + descending);
            }
            if (htmlAttribute != null)
            {
                foreach (var prop in htmlAttribute.GetType().GetProperties())
                {
                    if (prop.Name != "class")
                        tag.MergeAttribute(prop.Name.Replace("_", "-"), ConvertTo.String(prop.GetValue(htmlAttribute)));
                    else
                        tag.AddCssClass(ConvertTo.String(prop.GetValue(htmlAttribute)));
                }
            }
            HtmlString _htmlString = new HtmlString(tag.ToString());
            return _htmlString;
        }
    }
}