﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PMGDisha.Models.CustomModels
{
    public class Auth
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
    }
    public class LoginModel
    {
        [Required(ErrorMessage = "Enter username", AllowEmptyStrings = false)]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Enter password", AllowEmptyStrings = false)]
        public string Password { get; set; }
    }
}
