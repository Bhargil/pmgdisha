﻿using PMGDisha.Models.CustomModels;
using PMGDisha.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PMGDisha.Services
{
    public class AuthServices
    {
        public Auth GetAuth(string _userId, string _password)
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                return db.User.Where(w => w.UserId == _userId && w.Password == _password && w.Isactive == true).Select(s => new Auth
                {
                    UserId = s.Id,
                    UserName =s.UserId,
                    UserType = s.UserType

                }).FirstOrDefault();
            }
        }

        public List<User> GatUserDetails()
        {
            using (PMGDishaDBContext db = new PMGDishaDBContext())
            {
                return db.User.ToList();
            }
        }
    }
}
