﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PMGDisha.Models.EntityModels
{
    public partial class PMGDishaDBContext : DbContext
    {
        public PMGDishaDBContext()
        {
        }

        public PMGDishaDBContext(DbContextOptions<PMGDishaDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Businesspartner> Businesspartner { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=sql5023.site4now.net;Database=DB_A4A8F8_pmgdisha;Initial Catalog=DB_A4A8F8_pmgdisha;User Id=DB_A4A8F8_pmgdisha_admin;Password=N9KGevts5w;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Businesspartner>(entity =>
            {
                entity.ToTable("BUSINESSPARTNER");

                entity.HasIndex(e => e.AuthToken)
                    .HasName("UC_Auth")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AuthToken)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FkuserId).HasColumnName("FKUserId");

                entity.Property(e => e.MobileNo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SequenceNo)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Fkuser)
                    .WithMany(p => p.Businesspartner)
                    .HasForeignKey(d => d.FkuserId)
                    .HasConstraintName("FK__BUSINESSP__FKUse__1DE57479");
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.ToTable("CUSTOMERS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AadharNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FkbusinessPartnerId).HasColumnName("FKBusinessPartnerId");

                entity.Property(e => e.Image1Iurl)
                    .HasColumnName("Image1IURL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image1Url)
                    .HasColumnName("Image1URL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image2Iurl)
                    .HasColumnName("Image2IURL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image2Url)
                    .HasColumnName("Image2URL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image3Iurl)
                    .HasColumnName("Image3IURL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image3Url)
                    .HasColumnName("Image3URL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image4Iurl)
                    .HasColumnName("Image4IURL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Image4Url)
                    .HasColumnName("Image4URL")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MobileNo)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.RationNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VillageName).HasMaxLength(100);

                entity.HasOne(d => d.FkbusinessPartner)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.FkbusinessPartnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CUSTOMERS__FKBus__1BFD2C07");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("USER");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Createddate)
                    .HasColumnName("CREATEDDATE")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isactive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserType)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });
        }
    }
}
